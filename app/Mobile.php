<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\TelcoProviders\TelcoProvider1;
use App\Services\ContactService;

class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);
    
    if($contact == null) return;

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}
  
  public function sendSms(string $number, string $body)
	{
    if( empty($number) || empty($body) ) return;
    
    $isNumberValid = ContactService::validateNumber($number);
       
    if(!$isNumberValid) return;
    
    return $this->provider->sendMessage($number,$body);       
		
	}


}
