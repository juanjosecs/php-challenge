<?php

namespace App\Services;

use App\Contact;
use App\Services\ContactService;

class ContactService
{
	
	public static function findByName(string $name): ?Contact
	{
		// queries to the db
    return $name == "juanjose" ? new Contact() : null;
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
    return preg_match("/^[0-9]{9}$$/", $number);
	}
}