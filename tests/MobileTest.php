<?php

namespace Tests;

use App;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
    $provider = m::mock('App\Interfaces\CarrierInterface');
    
		$mobile = new App\Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}
 
  /** @test */
  public function it_returns_contact_when_exists()
	{
    $provider = new App\TelcoProviders\TelcoProvider1();
    
		$mobile = new App\Mobile($provider);

		$this->assertNotNull($mobile->makeCallByName('juanjose'));
	}
  
  /** @test */
  public function it_returns_contact_when_not_exists()
	{
    $provider = new App\TelcoProviders\TelcoProvider1();
    
		$mobile = new App\Mobile($provider);

		$this->assertNull($mobile->makeCallByName('juan jose'));
	}
    
  /** @test */
  public function test_sendSMS_when_number_valid()
	{
    $provider = new App\TelcoProviders\TelcoProvider1();
    
		$mobile = new App\Mobile($provider);
    
		$this->assertNotNull($mobile->sendSms("999999999","test sms message"));
	}
  
  /** @test */
  public function test_sendSMS_when_number_not_valid()
	{
    $provider = new App\TelcoProviders\TelcoProvider1();
    
		$mobile = new App\Mobile($provider);
    
		$this->assertNull($mobile->sendSms("999","test sms message"));
	}
  
  /** @test */
  public function test_sendSMS_when_not_paramters()
	{
    $provider = new App\TelcoProviders\TelcoProvider1();
    
		$mobile = new App\Mobile($provider);
    
		$this->assertNull($mobile->sendSms("",""));
	}
  
  /** @test */
  public function test_sendSMS_when_message_invalid()
	{
    $provider = new App\TelcoProviders\TelcoProvider1();
    
		$mobile = new App\Mobile($provider);
    
		$this->assertNull($mobile->sendSms("999999999",""));
	}

}
